import jsonQuery from 'json-query'
import Tools from './Tools'

import ItemJSON from '../interface/ItemJSON'
import ItemSetJSON from '../interface/ItemSetJSON'
import EnchantJSON from '../interface/EnchantJSON'
import ItemSlot from '../enum/ItemSlot'

import Faction from '../enum/Faction'
import PvPRank from '../enum/PvPRank'

import items from '../db/items.json'
import enchants from '../db/enchants.json'
import itemSets from '../db/itemSets.json'
import WeaponSubclass from '../enum/WeaponSubclass'

interface ItemQuery {
  id?: number
  customId?: string
  name?: string
  slot?: ItemSlot
  phase?: number
  faction?: Faction
  pvpRank?: PvPRank
  worldBosses?: boolean
  raids?: boolean
  randomEnchants?: boolean
  cloneResults?: boolean
  equippedWeaponSubclass?: WeaponSubclass
}

interface SpellQuery {
  name?: string
  baseName?: string
  rank?: number
  phase?: number
  cloneResults?: boolean
}

// Stats displayed on the gear item select table
const DISPLAYED_STATS_ON_ITEM_SELECT = [
  'mitScore',
  'threatScore',
  'score',
  'stamina',
  'armor',
  'strength',
  'agility',
  'meleeCrit',
  'meleeHit',
  'attackPower',
  'defense',
  'dodge',
  'haste',
  'feralAttackPower'
]

export default class Query {
  /* return input, deep clone it if cloneResults is true */
  static _result(o: any, cloneResults: boolean) {
    if (cloneResults) {
      return Tools.CloneObject(o ? o : {})
    }
    return o ? o : {}
  }

  static Item(opts: ItemQuery): ItemJSON | undefined {
    const items = this.Items(opts)
    if (items && items[0]) {
      return items[0]
    }
    return undefined
  }

  static Items(opts: ItemQuery): ItemJSON[] {
    const noRandomEnchants = (itemJSON: ItemJSON) => {
      if (!itemJSON || !itemJSON.customId) {
        return true
      }

      return Tools.isLetter(itemJSON.customId.charAt(0)) ? false : true
    }

    const slot2query = (slot: ItemSlot) => {
      switch (slot) {
        case ItemSlot.Finger2:
          return `[* slot=${ItemSlot.Finger}]`
        case ItemSlot.Trinket2:
          return `[* slot=${ItemSlot.Trinket}]`
        case ItemSlot.Mainhand:
          return `[* slot=${ItemSlot.Mainhand} | slot=${ItemSlot.Onehand} | slot=${ItemSlot.Twohand}]`
        case ItemSlot.Onehand:
          return `[* slot=${ItemSlot.Mainhand} | slot=${ItemSlot.Onehand}]`
        default:
          return `[* slot=${slot}]`
      }
    }

    const singleItemQuery = (query: string): ItemJSON[] => {
      const result: ItemJSON[] = []
      const x = jsonQuery(query, { data: items }).value
      if (x) {
        result.push(x)
      }

      return this._result(result, opts.cloneResults ? opts.cloneResults : false)
    }

    /* id, customId and name are unique. if one is passed just lookup and return */
    if (opts.id) {
      return singleItemQuery(`[id=${opts.id}]`)
    } else if (opts.customId) {
      return singleItemQuery(`[customId=${opts.customId}]`)
    } else if (opts.name) {
      return singleItemQuery(`[name=${opts.name}]`)
    }

    let result: ItemJSON[] = []

    /* at this point if we don't have slot just return an empty set. we don't really
     * have a use-case for returning array of items from different slots */
    if (opts.slot === undefined) {
      return result
    }

    result = jsonQuery(slot2query(opts.slot), { data: items }).value

    if (opts.faction !== undefined) {
      result = jsonQuery(`[* faction = ${opts.faction} | faction = ${Faction.Horde | Faction.Alliance}]`, {
        data: result
      }).value
    }

    if (opts.phase !== undefined) {
      result = jsonQuery(`[* phase <= ${opts.phase}]`, { data: result }).value
    }

    if (opts.pvpRank !== undefined) {
      result = jsonQuery(`[* pvpRank <= ${opts.pvpRank}]`, { data: result }).value
    }

    if (opts.worldBosses !== undefined && opts.worldBosses === false) {
      result = jsonQuery(`[* worldBoss = false ]`, { data: result }).value
    }

    if (opts.raids !== undefined && opts.raids === false) {
      result = jsonQuery(`[* raid = false ]`, { data: result }).value
    }

    if (opts.randomEnchants !== undefined && opts.randomEnchants === false) {
      result = result.filter(noRandomEnchants)
    }

    const results = this._result(result, opts.cloneResults ? opts.cloneResults : false)
    // If a stat is missing on the ItemJson set it to an empty string so 'undefined' doesn't get displayed on the item table
    if (results) {
      for (const r of results) {
        for (const key of DISPLAYED_STATS_ON_ITEM_SELECT) {
          r[key] = r[key] !== undefined ? r[key] : ''
        }
      }
    }
    return results
  }

  static ItemSet(opts: ItemQuery): ItemSetJSON | undefined {
    const itemSets = this.ItemSets(opts)
    if (itemSets && itemSets[0]) {
      return itemSets[0]
    }
    return undefined
  }

  static ItemSets(opts: ItemQuery): ItemSetJSON[] {
    const singleItemSetQuery = (query: string): ItemSetJSON[] => {
      const result: ItemSetJSON[] = []
      const x = jsonQuery(query, { data: itemSets }).value
      if (x) {
        result.push(x)
      }
      return this._result(result, opts.cloneResults ? opts.cloneResults : false)
    }

    let result: ItemSetJSON[] = []

    if (opts.name) {
      result = singleItemSetQuery(`[name=${opts.name}]`)
    } else {
      result = jsonQuery(``, { data: itemSets }).value
    }

    if (opts.phase !== undefined) {
      result = jsonQuery(`[* phase <= ${opts.phase}]`, { data: result }).value
    }

    if (opts.raids !== undefined && opts.raids === false) {
      result = jsonQuery(`[* raid = false ]`, { data: result }).value
    }

    return this._result(result, opts.cloneResults ? opts.cloneResults : false)
  }

  static Enchant(opts: ItemQuery): EnchantJSON | undefined {
    const enchants = this.Enchants(opts)
    if (enchants && enchants[0]) {
      return enchants[0]
    }
    return undefined
  }

  static Enchants(opts: ItemQuery): EnchantJSON[] {
    const singleEnchantQuery = (query: string): EnchantJSON[] => {
      const result: EnchantJSON[] = []
      const x = jsonQuery(query, { data: enchants }).value
      if (x) {
        result.push(x)
      }
      return this._result(result, opts.cloneResults ? opts.cloneResults : false)
    }

    const applicableWeaponEnchant = (enchantJson: EnchantJSON) => {
      if (enchantJson.applicableWeaponSubclasses !== undefined && opts.equippedWeaponSubclass !== undefined) {
        return enchantJson.applicableWeaponSubclasses.includes(opts.equippedWeaponSubclass)
      }
      return true
    }

    /* id and name are unique. if one is passed just lookup and return */
    if (opts.id) {
      return singleEnchantQuery(`[id=${opts.id}]`)
    } else if (opts.customId) {
      return singleEnchantQuery(`[customId=${opts.customId}]`)
    } else if (opts.name) {
      return singleEnchantQuery(`[name=${opts.name}]`)
    }

    let result: EnchantJSON[] = []

    if (opts.slot === undefined) {
      return result
    }

    result = jsonQuery(`[* slot = ${opts.slot} | slot = -2 ]`, { data: enchants }).value

    if (opts.phase !== undefined) {
      result = jsonQuery(`[* phase <= ${opts.phase}]`, { data: result }).value
    }
    if (opts.equippedWeaponSubclass !== undefined) {
      result = result.filter(applicableWeaponEnchant)
    }

    const results = this._result(result, opts.cloneResults ? opts.cloneResults : false)
    // If a stat is missing on the ItemJson set it to an empty string so 'undefined' doesn't get displayed on the item table
    if (results) {
      for (const r of results) {
        for (const key of DISPLAYED_STATS_ON_ITEM_SELECT) {
          r[key] = r[key] !== undefined ? r[key] : ''
        }
      }
    }
    return results
  }
}
