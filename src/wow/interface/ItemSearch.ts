import TargetType from '../enum/TargetType'
import Faction from '../enum/Faction'
import PvPRank from '../enum/PvPRank'
import MagicSchool from '../enum/MagicSchool'
import SortOrder from '../enum/SortOrder'
import ItemSlot from '../enum/ItemSlot'

import LockedItems from './LockedItems'
import LockedEnchants from './LockedEnchants'
import WeaponSubclass from '../enum/WeaponSubclass'
import Weights from './Weights'

export default interface ItemSearch {
  phase: number
  faction: Faction
  pvpRank: PvPRank
  raids: boolean
  worldBosses: boolean
  randomEnchants: boolean
  encounterLength: number
  onUseItems: boolean
  targetType: TargetType
  lockedItems?: LockedItems
  lockedEnchants?: LockedEnchants
  slot: ItemSlot
  sortOrder?: SortOrder
  equippedWeaponSubclass?: WeaponSubclass
}
