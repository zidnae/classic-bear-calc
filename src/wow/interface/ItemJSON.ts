import ItemSlot from '../enum/ItemSlot'
import ItemQuality from '../enum/ItemQuality'
import ItemClass from '../enum/ItemClass'
import ArmorSubclass from '../enum/ArmorSubclass'
import WeaponSubclass from '../enum/WeaponSubclass'
import PlayableClass from '../enum/PlayableClass'
import Faction from '../enum/Faction'
import PvPRank from '../enum/PvPRank'
import TargetType from '../enum/TargetType'
import ItemOnUseJSON from './ItemOnUseJSON'

export default interface ItemJSON {
  slot: ItemSlot
  pvpRank: PvPRank
  raid: boolean
  worldBoss: boolean
  id?: number
  customId?: string
  name?: string
  class?: ItemClass
  subclass?: ArmorSubclass | WeaponSubclass
  quality?: ItemQuality
  level?: number
  reqLevel?: number
  bop?: boolean
  unique?: boolean
  allowableClasses?: PlayableClass[]
  targetTypes?: TargetType
  phase?: number
  icon?: string
  location?: string
  boss?: string
  faction?: Faction
  mitScore?: number
  threatScore?: number
  score?: number
  onUseThreatScore?: number
  onUseMitScore?: number
  spellDamage?: number
  arcaneDamage?: number
  natureDamage?: number
  spellHealing?: number
  spellHit?: number
  spellCrit?: number
  spellPenetration?: number
  stamina?: number
  intellect?: number
  spirit?: number
  mp5?: number
  armor?: number
  durability?: number
  minDmg?: number
  maxDmg?: number
  speed?: number
  dps?: number
  onUse?: ItemOnUseJSON
  strength?: number
  agility?: number
  meleeCrit?: number
  meleeHit?: number
  bonusWeaponDamage?: number
  attackPower?: number
  defense?: number
  dodge?: number
  haste?: number
  feralAttackPower?: number
  bonusArmor?: number
  bonusHealth?: number
}
