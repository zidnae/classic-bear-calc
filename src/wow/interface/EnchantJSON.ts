import ItemSlot from '../enum/ItemSlot'

export default interface EnchantJSON {
  id: number
  customId: string
  name: string
  slot: ItemSlot
  phase: number
  icon: string
  score: number
  mitScore: number
  threatScore: number
  onUseThreatScore?: number
  onUseMitScore?: number
  text: string
  spellHealing: number
  bonusArmor: number
  spellDamage: number
  arcaneDamage: number
  natureDamage: number
  spellHit: number
  spellCrit: number
  spellPenetration: number
  stamina: number
  intellect: number
  spirit: number
  mp5: number
  defense: number
  strength: number
  agility: number
  haste: number
  attackPower: number
  meleeCrit: number
  meleeHit: number
  bonusHealth: number
  dodge: number
  armor: number
  feralAttackPower: number
  applicableWeaponSubclasses?: Array<number>
}
