export default interface Weights {
  armor: number
  strength: number
  agility: number
  stamina: number
  meleeHit: number
  meleeCrit: number
  attackPower: number
  dodge: number
  defense: number
  haste: number
  bonusArmor: number
  bonusHealth: number
  feralAttackPower: number
}
