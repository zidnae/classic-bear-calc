import TargetType from '../enum/TargetType'

export default interface OptionsTarget {
  level: number
  debuffs: string[]
  type: TargetType
  baseArmor: number
  numTargets: number
}
