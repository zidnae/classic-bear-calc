import OptionsCharacter from './OptionsCharacter'
import OptionsTarget from './OptionsTarget'
import ItemSlot from '../enum/ItemSlot'

export default interface Options {
  debug: boolean
  experimental: boolean
  phase: number
  raids: boolean
  worldBosses: boolean
  randomEnchants: boolean
  onUseItems: boolean
  encounterLength: number
  desiredStaminaAPRatio: number
  unmitagatedDamageTakenPerSecond: number
  ehpDodgeWeight: number
  itemSearchSlot: ItemSlot
  enchantSearchSlot: ItemSlot
  character: OptionsCharacter
  target: OptionsTarget
}
