import OptionsTarget from '../interface/OptionsTarget'
import { Buff, sumBuffStatBonuses, BuffStatBonuses } from './Buffs'

declare type BuffFlagType = keyof typeof Buff

export default class Target {
  options: OptionsTarget
  debuffFlags: Array<Buff>
  buffStatBonuses: BuffStatBonuses

  constructor(options: OptionsTarget) {
    this.options = options
    this.debuffFlags = options.debuffs.map((b) => Buff[b as BuffFlagType])
    this.buffStatBonuses = sumBuffStatBonuses(this.options.debuffs)
  }

  get level(): number {
    return this.options.level
  }

  get armor(): number {
    return Math.max(this.options.baseArmor + this.buffStatBonuses.armor, 0)
  }

  get defenseSkill(): number {
    return this.level * 5
  }
}
