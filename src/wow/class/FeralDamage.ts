import AttackTable from '../interface/AttackTable'
import { toInteger } from 'lodash'
import { strict as assert } from 'assert'

const BASE_PAW_ATTACK_SPEED = 2.5 // attacks per sec in bear form
const MAUL_BONUS_DAMAGE = 128 // Flat extra damage from maul
const BASE_PAW = 127 // Base average damage of bear form auto attack
const GCD_SECS = 1.5 // min time in between abilities enforced by the GCD

/// increases damage caused by maul, swipe, claw, and rake by
function savageFuryDamageMultipler(talentPts: number): number {
  return 1 + 0.1 * talentPts
}

/// Increases damage dealt in all forms with Physical attacks by
function naturalWeaponsAttackDamageMultiplier(talentPts: number): number {
  return 1 + 0.02 * talentPts
}

/// Computes average auto-attack damage in bear form
export function averageBearAutoAttackDamage(
  attackPower: number,
  playerAttackTable: AttackTable,
  naturalWeaponTalentPts: number,
  worldBuffDamageMultiplier: number
): number {
  return toInteger(
    (BASE_PAW + (attackPower / 14) * BASE_PAW_ATTACK_SPEED) *
      naturalWeaponsAttackDamageMultiplier(naturalWeaponTalentPts) *
      worldBuffDamageMultiplier *
      playerAttackTable.attackModifier
  )
}

interface SwipeDamageInputs {
  playerAttackTable: AttackTable
  naturalWeaponTalentPts: number
  savageFuryTalentPts: number
  worldBuffDamageMultiplier: number
}

export function swipeDamage({
  playerAttackTable,
  naturalWeaponTalentPts,
  savageFuryTalentPts,
  worldBuffDamageMultiplier
}: SwipeDamageInputs): number {
  const BASE_SWIPE_DAMAGE = 83
  return (
    BASE_SWIPE_DAMAGE *
    naturalWeaponsAttackDamageMultiplier(naturalWeaponTalentPts) *
    worldBuffDamageMultiplier *
    playerAttackTable.attackModifier *
    savageFuryDamageMultipler(savageFuryTalentPts)
  )
}

interface SheetRageGeneratedPerSecondInputs {
  mitigatedDTPS: number
  maulRageCost: number
  swipeRageCost: number
  playerAttackTable: AttackTable
  swingsPerSecond: number
  primalFuryRank: number
  autoAttackDPS: number
}

export interface SheetRageModelOutputs {
  ragePerSecFromAutoAttacks: number
  ragePerSecFromSwipe: number
  ragePerSecondFromDamageTaken: number
  swipesPerSecond: number
  maulUptime: number
  swipeUptime: number
}

/**
 * https://blue.mmo-champion.com/topic/18325-the-new-rage-formula-by-kalgan/
 * Here are the current rage generation formulae (note: these are slightly different than the ones which are on the current public test realms).
 *
 * The notable changes include an update to offhand rage generation (previously it was not being normalized correctly), and an improvement to rage generation from crits (essentially, crits now generate double the rage the hit would have ordinarily caused). The later change should help ensure that certain specs (ie: sword specialization) don't become clearly superior to crit-enhancing specializations like Axe/Polearm.
 *
 * For Dealing Damage:
 * Main Hand Normal Hits: Factor=2.5
 * Main Hand Crits: Factor=5.0
 * Off Hand Normal Hits: Factor=1.25
 * Off Hand Crits: Factor=2.5
 *
 * Rage Conversion Value (note: this number is derived from other values within the game such as a mob's hit points and a warrior's expected damage value against that mob):
 *
 * Rage Conversion at level 60: 230.6
 * Rage Conversion at level 70: 274.7
 *
 * Expansion Rage Gained from dealing damage = ((Damage Dealt) / (Rage Conversion at Your Level) * 7.5 + (Weapon Speed * Factor))/2
 *
 * Pre-Expansion Rage Gained from dealing damage = (Damage Dealt) / (Rage Conversion at Your Level) * 7.5
 *
 * For Taking Damage (both pre and post expansion):
 * Rage Gained = (Damage Taken) / (Rage Conversion at Your Level) * 2.5
 */
export function sheetRageModel({
  mitigatedDTPS,
  maulRageCost,
  swipeRageCost,
  playerAttackTable,
  swingsPerSecond,
  primalFuryRank,
  autoAttackDPS
}: SheetRageGeneratedPerSecondInputs): SheetRageModelOutputs {
  const ragePerSecondFromIncomingDamage = (mitigatedDTPS / 230.6) * 2.5
  const discountedMaulRageCostFromMisses =
    (1 - playerAttackTable.dodge - playerAttackTable.parry - playerAttackTable.miss) * maulRageCost +
    (playerAttackTable.dodge + playerAttackTable.parry + playerAttackTable.miss) * Math.round(0.2 * maulRageCost)
  const ragePerSecFromPrimalFury = playerAttackTable.criticalHit * swingsPerSecond * 0.5 * primalFuryRank * 5
  const ragePerSecondRequiredForFullMaulUptime = swingsPerSecond * discountedMaulRageCostFromMisses
  const ragePerSecondGainedWhenAutoAttacking = (autoAttackDPS / 230.6) * 7.5
  const maulUptime = Math.min(
    (ragePerSecondFromIncomingDamage + ragePerSecFromPrimalFury) / ragePerSecondRequiredForFullMaulUptime,
    1
  )
  const leftOverRagePerSecondForSwipe =
    maulUptime >= 1
      ? ragePerSecondFromIncomingDamage +
        ragePerSecFromPrimalFury +
        ragePerSecondGainedWhenAutoAttacking * (1 - maulUptime) -
        ragePerSecondRequiredForFullMaulUptime * maulUptime
      : 0
  const rageRefundedPerSwipeFromPrimalFury = playerAttackTable.criticalHit * 0.5 * primalFuryRank * 5
  const swipesPerSecondFromExcessRage = Math.min(
    leftOverRagePerSecondForSwipe / (swipeRageCost - rageRefundedPerSwipeFromPrimalFury),
    1 / GCD_SECS
  )
  const swipeUptime = swipesPerSecondFromExcessRage / (1 / GCD_SECS)
  return {
    ragePerSecFromAutoAttacks: ragePerSecFromPrimalFury,
    ragePerSecFromSwipe: rageRefundedPerSwipeFromPrimalFury * swipesPerSecondFromExcessRage,
    swipesPerSecond: swipesPerSecondFromExcessRage,
    maulUptime: maulUptime,
    ragePerSecondFromDamageTaken: ragePerSecondFromIncomingDamage,
    swipeUptime: swipeUptime
  }
}

enum FeralSpell {
  AutoAttack,
  Maul,
  Swipe,
  FeralFaerieFire
}

enum SpellStatus {
  Miss,
  Dodge,
  Parry,
  OrdinaryHit,
  CriticalHit
}

interface SimEvent {
  spell: FeralSpell
  status: SpellStatus // miss, crit, etc
  damage: number // miss, crit, etc
}

export function feralSim(
  attackTimes: Array<number>,
  autoAttackDamage: number,
  maulDamage: number,
  playerAttackTable: AttackTable, // for normal attacks
  swipeDamage: number,
  improvedEnrageRank: number,
  encounterDurationSecs: number, //
  furorRank: number, //
  maulRageCost: number, //
  swipeRageCost: number //
): Array<SimEvent> {
  // Assume
  let rage = _rageGeneratedFromEnrage(improvedEnrageRank) + furorRank * 2
  const spellBatchTimes = linspace(0, encounterDurationSecs, 0.4)
  console.log(spellBatchTimes)
  const GCD_TIME = 1.5
  const gcdTimes = linspace(0, encounterDurationSecs, GCD_TIME)
  let attackTimeIndex = 0
  let gcdIndex = 0
  const events: Array<SimEvent> = []
  const feralFaerieFire = false
  for (const spellBatch of spellBatchTimes) {
    if (attackTimes[attackTimeIndex] >= spellBatch - 0.4 && attackTimes[attackTimeIndex] <= spellBatch) {
      attackTimeIndex += 1
      if (rage >= maulRageCost) {
        events.push({
          spell: FeralSpell.Maul,
          status: SpellStatus.OrdinaryHit,
          damage: maulDamage
        })
        rage -= maulRageCost
      } else {
        events.push({
          spell: FeralSpell.AutoAttack,
          status: SpellStatus.OrdinaryHit,
          damage: autoAttackDamage
        })
        rage += _rageGeneratedFromWhiteAttack(autoAttackDamage)
      }
      // do attack
    }
    if (gcdTimes[gcdIndex] >= spellBatch - 0.4) {
      gcdIndex += 1
      if (rage > 40) {
        events.push({
          spell: FeralSpell.Swipe,
          status: SpellStatus.OrdinaryHit,
          damage: swipeDamage
        })
        rage -= swipeRageCost
      } else if (feralFaerieFire) {
        /// Do faerie fire stuff
      }
    }
  }
  return events
}

function _rageGeneratedFromWhiteAttack(
  damage: number,
  rageConversionFactor: number = 0,
  weaponSpeed: number = 0,
  hitFactor: number = 0
): number {
  return (damage / 230.6) * 7.5
}

function _rageGeneratedFromEnrage(improvedEnrageRank: number): number {
  return 20 + improvedEnrageRank * 5
}

function linspace(startValue: number, stopValue: number, cardinality: number) {
  const arr = []
  let val = startValue
  while (val < stopValue) {
    arr.push(val)
    val += cardinality
  }
  return arr
}

interface MaulDamageInputs {
  attackPower: number
  playerAttackTable: AttackTable
  naturalWeaponTalentPts: number
  savageFuryTalentPts: number
  worldBuffDamageMultiplier: number
}

/// Computes average maul
export function maulDamage({
  attackPower,
  playerAttackTable,
  naturalWeaponTalentPts,
  savageFuryTalentPts,
  worldBuffDamageMultiplier
}: MaulDamageInputs): number {
  return (
    (BASE_PAW + MAUL_BONUS_DAMAGE + (attackPower / 14) * BASE_PAW_ATTACK_SPEED) *
    naturalWeaponsAttackDamageMultiplier(naturalWeaponTalentPts) *
    savageFuryDamageMultipler(savageFuryTalentPts) *
    worldBuffDamageMultiplier *
    playerAttackTable.attackModifier
  )
}

interface OnUseAttackSpeedItem {
  duration: number
  cooldown: number
  hasteModifier: number // greater than 1 e.g. 1% haste == 1.01
}

export function computeAttackTimes(
  attackSpeed: number, // secs
  encounterDuration: number, // secs
  onUseAttackSpeedItems: Array<OnUseAttackSpeedItem>
): Array<number> {
  let currentTime = 0
  const attackTimes: Array<number> = [currentTime]
  while (currentTime <= encounterDuration) {
    let currentAttackSpeed = attackSpeed
    for (const onUseItem of onUseAttackSpeedItems) {
      if (currentTime % (onUseItem.duration + onUseItem.cooldown) <= onUseItem.duration) {
        assert(onUseItem.hasteModifier >= 1, `Invalid haste modifier ${onUseItem.hasteModifier}`)
        currentAttackSpeed = currentAttackSpeed / onUseItem.hasteModifier
      }
    }
    currentTime = currentTime + currentAttackSpeed
    if (currentTime <= encounterDuration) {
      attackTimes.push(currentTime)
    }
  }
  return attackTimes
}

export default { averageBearAutoAttackDamage, computeAttackTimes, sheetRageModel }
