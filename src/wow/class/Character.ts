import constants from '../module/Constants'
import Equipment from './Equipment'

import PlayableRace from '../enum/PlayableRace'
import Faction from '../enum/Faction'
import {
  Buff,
  sumBuffStatBonuses,
  inspirationArmorModifer,
  improvedLayOnHandsArmorModifier,
  blessingOfKingsStatModifier,
  spiritOfZandalarStatModifier,
  moldarsMoxieStamModifier,
  BuffStatBonuses
} from './Buffs'

import OptionsCharacter from '../interface/OptionsCharacter'
import { strict as assert } from 'assert'
import ItemJSON from '../interface/ItemJSON'
import ItemSlot from '../enum/ItemSlot'
import TargetType from '../enum/TargetType'
import Weights from '../interface/Weights'

// Attacks per second
const BEAR_BASE_AUTO_ATTACK_SPEED = 2.5
const BEAR_FORM_THREAT_MODIFIER = 1.3

// Base chance a druid has to dodge an attack
// https://vanilla-wow.fandom.com/wiki/Dodge#:~:text=Your%20chance%20to%20Dodge%20an,Attacker's%20attack%20skill)%20*%200.04)
const BASE_DRUID_DOGE_CHANCE = 0.009

const BASE_CRIT_CHANCE = 0.009

declare type BuffFlagType = keyof typeof Buff

// https://classicwow.live/guides/46/basic-stats-sheet#class_and_race_level_60_base_stats
// Character base stats at 60
const TAUREN_BASE_STATS = {
  strength: 70,
  agility: 55,
  stamina: 72,
  intellect: 95,
  spirit: 112
}

const NIGHT_ELF_BASE_STATS = {
  strength: 62,
  agility: 65,
  stamina: 69,
  intellect: 100,
  spirit: 110
}

const BEAR_FORM_ATTACK_POWER_BONUS = 180
const BEAR_FORM_HEALTH_BONUS = 1240

/**
 * Stores character attributes, Talents, Gear, and Buffs
 */
export default class Character {
  options: OptionsCharacter
  equipment: Equipment
  buffList: Array<Buff>
  buffStatBonuses: BuffStatBonuses

  constructor(options: OptionsCharacter, equipment: Equipment) {
    this.options = options
    this.equipment = equipment
    this.buffList = options.buffs.map((b) => Buff[b as BuffFlagType])
    this.buffStatBonuses = sumBuffStatBonuses(this.options.buffs)
  }

  hasBuff(buff: Buff) {
    return this.buffList.includes(buff)
  }

  static factionFromRace(race: PlayableRace): Faction {
    switch (race) {
      case PlayableRace.Tauren:
      case PlayableRace.Orc:
      case PlayableRace.Undead:
      case PlayableRace.Troll:
        return Faction.Horde
      default:
        return Faction.Alliance
    }
  }

  get level(): number {
    return this.options.level
  }

  get faction(): Faction {
    return Character.factionFromRace(this.options.race)
  }

  get isHorde(): boolean {
    return (this.faction & Faction.Horde) === Faction.Horde
  }

  get isAlliance(): boolean {
    return (this.faction & Faction.Alliance) === Faction.Alliance
  }

  get isTauren(): boolean {
    return this.options.race === PlayableRace.Tauren
  }

  get isNightElf(): boolean {
    return this.options.race === PlayableRace.NightElf
  }

  get health(): number {
    const BASE_LVL_60_DRUID_HEALTH = 1299
    return (
      (BASE_LVL_60_DRUID_HEALTH +
        BEAR_FORM_HEALTH_BONUS +
        10 * this.stamina +
        this.buffStatBonuses.bonusHealth +
        this.equipment.bonusHealth) *
      this.hpModifier
    )
  }

  get hpModifier(): number {
    return this.isTauren ? 1.05 : 1
  }
  // Returns the health granted by one point of stam
  stamToHealthRatio(): number {
    return this._stamStatModifer() * (this.isTauren ? 1.05 : 1) * 10
  }

  get mana(): number {
    return 964 + 15 * this.intellect
  }

  // Modifiers to apply from stamina (from things like Heart of the Wild) that converts 1 pt of stamina from armor to effective stamina
  _stamStatModifer(): number {
    return (
      (this.hasBuff(Buff.MoldarMoxie) ? moldarsMoxieStamModifier() : 1) *
      this._buffStatMultiplier *
      (this.options.talents.heartOfTheWildRank * 0.04 + 1)
    )
  }

  get stamina(): number {
    /// Add stam talent bonus
    return (
      ((this.isNightElf ? NIGHT_ELF_BASE_STATS.stamina : TAUREN_BASE_STATS.stamina) +
        this.equipment.stamina +
        this.buffStatBonuses.stamina) *
      this._stamStatModifer()
    )
  }

  get maulRageCost(): number {
    const maulRageCost = 15 - this.options.talents.ferocityRank
    return this.equipment.idol.name === 'Idol of Brutality' ? maulRageCost - 3 : maulRageCost
  }

  get swipeRageCost(): number {
    const swipeRageCost = 20 - this.options.talents.ferocityRank
    return this.equipment.idol.name === 'Idol of Brutality' ? swipeRageCost - 3 : swipeRageCost
  }

  get _buffStatMultiplier(): number {
    return (
      (this.hasBuff(Buff.SpiritOfZandalar) ? spiritOfZandalarStatModifier() : 1) *
      (this.hasBuff(Buff.BlessingOfKings) ? blessingOfKingsStatModifier() : 1)
    )
  }

  // Average attack speed over the couse of a fight
  sheetWeaponSpeed(): number {
    return BEAR_BASE_AUTO_ATTACK_SPEED / (this.equipment.haste * (this.hasBuff(Buff.WarchiefsBlessing) ? 1.15 : 1))
  }

  get feralInstinctThreatModifier(): number {
    return 0.03 * this.options.talents.feralInstinctRank
  }

  get maulThreatModifier(): number {
    return this.baseThreatModifier * 1.75
  }

  get swipeThreatModifier(): number {
    return this.baseThreatModifier * 1.75
  }

  get baseThreatModifier(): number {
    // The Bear Form and Feral Instinct threat modifers stack additively while the threat enchant
    //  stacks multiplicitively. This is acknowledged inconsistent behavior
    //  (Fun fact: The warriors threat analogous increasing talent stacks multiplicitively
    //   with defensive stance as if bears didn't have it bad enough already)
    return (
      (BEAR_FORM_THREAT_MODIFIER + this.feralInstinctThreatModifier) *
      /// 'Enchant Gloves - Threat'
      (this.equipment.hands.enchantId === 25072 ? 1.02 : 1)
    )
  }

  get bearForm(): boolean {
    return true
  }

  /**
   *
   * Static Armor Buffs (Elixir, Pot, MotW, DevAura) do stack with each other and also with the Multiplicative Armor Buffs (Inspiration/AF and LoH), but do not benefit from the multiplication.
   * (This behaviour is unlike Static and Multiplicative Buffs to other Stats, which are always added first and multiplied second.)
   *
   * In other words, Inspiration and LoH are calculated first, and all Static Armor Buffs added second.
   *
   * Interestingly, Lay on Hands is calculated from base Armor which doesn't include Armor from Agility (x2), while Inspiration does multiplies the Armor from Agility. This correlates with the database modifier tags of each Aura: "Mod Resistance" for Inspiration vs "Mod Base Resistance" for LoH.
   * https://classic.wowhead.com/spell=15359/inspiration
   * https://classic.wowhead.com/spell=20236/lay-on-hands
   *
   * Armor = Static Buffs  +  ( BaseArmor X DireBearForm X ThickHide X LayOnHands + TotalAgility X 2) X Inspiration
   *
   * The better visualisation of this formula: https://discord.com/channels/253205420225724416/253206574787461120/770735214971453500
   *
   * Source: https://discordapp.com/channels/253205420225724416/253206574787461120/770732696325324810
   */
  get armor(): number {
    // Bear form gives 360% + armor so 4.6 as its 1 from the armor plus the 360% benefit
    return (
      (this.equipment.armor * this.equipmentArmorModifier + this.agility * 2) * this.agiAndEquipmentArmorModifier +
      this.equipment.bonusArmor +
      this.buffStatBonuses.bonusArmor
    )
  }

  // Modifier applied to both armor from agi and armor from equipped items ( and not from buffs/potions )
  get agiAndEquipmentArmorModifier(): number {
    return this.hasBuff(Buff.Inspiration) ? inspirationArmorModifer() : 1
  }

  // Modifer applied to 1 armor on an equipped item (as opposed to bonus armor from potions/buffs)
  get equipmentArmorModifier(): number {
    return (
      4.6 * this.thickHideArmorBenefit * (this.hasBuff(Buff.ImprovedLayOnHands) ? improvedLayOnHandsArmorModifier() : 1)
    )
  }

  // Player dodge chance as would be displayed on you in game character sheet
  // not including dodge gained from +Defense
  get dodge(): number {
    return (
      this.equipment.dodge +
      this.agility / 20 / 100 +
      // Night elf 1% dodge racial
      (this.isNightElf ? 0.01 : 0) +
      BASE_DRUID_DOGE_CHANCE +
      this.buffStatBonuses.dodge
    )
  }

  get defense(): number {
    return this.level * 5 + this.equipment.defense + this.buffStatBonuses.defense
  }

  get agility(): number {
    return (
      ((this.isNightElf ? NIGHT_ELF_BASE_STATS.agility : TAUREN_BASE_STATS.agility) +
        this.equipment.agility +
        this.buffStatBonuses.agility) *
      this._buffStatMultiplier
    )
  }

  get strength(): number {
    return (
      ((this.isNightElf ? NIGHT_ELF_BASE_STATS.strength : TAUREN_BASE_STATS.strength) +
        this.equipment.strength +
        this.buffStatBonuses.strength) *
      this._buffStatMultiplier
    )
  }

  attackPower(targetType?: TargetType): number {
    const hasSealofTheDawn =
      this.equipment.trinket.name === 'Seal of the Dawn' || this.equipment.trinket2.name === 'Seal of the Dawn'

    const hasMarkoftheChampion =
      this.equipment.trinket.name === 'Mark of the Champion' || this.equipment.trinket2.name === 'Mark of the Champion'

    // Creating a lvl 1 character and calculating the expected AP from str
    //  vs the AP reported by extended character stats is looks
    //  as though the first 20 AP from base stats is ignored
    const BASE_AP_KNOCKDOWN = -20

    return (
      BASE_AP_KNOCKDOWN +
      BEAR_FORM_ATTACK_POWER_BONUS +
      this.equipment.attackPower +
      this.equipment.feralAttackPower +
      this.options.talents.predatoryStrikesRank * 0.5 * this.level +
      this.strength * 2 +
      this.buffStatBonuses.attackPower +
      this.buffStatBonuses.feralAttackPower +
      (targetType === TargetType.Undead && hasSealofTheDawn ? 81 : 0) +
      ((targetType === TargetType.Undead || targetType === TargetType.Demon) && hasMarkoftheChampion ? 150 : 0)
    )
  }

  get meleeHit(): number {
    const hit = this.equipment.meleeHit + this.buffStatBonuses.meleeHit
    assert(hit <= 1, `Invalid melee hit of ${hit} over 1`)
    return hit
  }

  get meleeCrit(): number {
    const crit =
      BASE_CRIT_CHANCE +
      this.equipment.meleeCrit +
      this.agility / 20 / 100 +
      this.buffStatBonuses.meleeCrit +
      (this.options.talents.leaderOfThePackRank ? 0.03 : 0) +
      this.options.talents.sharpenedClawsRank * 0.02
    assert(crit <= 1 && crit >= 0, `Invalid crit of ${crit}`)
    return crit
  }

  get weaponSkill(): number {
    /// Feral druids have now way to modify weapon skill and it's always maxxed in form
    return this.level * 5
  }

  get intellect(): number {
    return (
      ((this.isNightElf ? 100 : 95) + this.equipment.intellect + this.buffStatBonuses.intellect) *
      this._buffStatMultiplier *
      (this.options.talents.heartOfTheWildRank * 0.04 + 1)
    )
  }

  get spirit(): number {
    return (
      ((this.isNightElf ? 110 : 112) + this.equipment.spirit + this.buffStatBonuses.spirit) * this._buffStatMultiplier
    )
  }

  get mp5(): number {
    return this.equipment.mp5 + this.buffStatBonuses.mp5
  }

  get manaPerTickNotCasting(): number {
    const fromBase = (15 * this.level) / 60
    const fromSpirit = this.spirit / 5
    const fromMp5 = this.mp5 ? (this.mp5 / 5) * 2 : 0

    return fromBase + fromSpirit + fromMp5
  }

  get manaPerTickCasting(): number {
    const fromBase = ((15 * this.level) / 60) * this.reflectionBonus
    const fromSpirit = (this.spirit / 5) * this.reflectionBonus
    const fromMp5 = this.mp5 ? (this.mp5 / 5) * 2 : 0

    return fromBase + fromSpirit + fromMp5
  }

  get manaPerTickInnervate(): number {
    return this.manaPerTickNotCasting * 4
  }

  get manaPerInnervate(): number {
    return this.manaPerTickInnervate * 10
  }

  get spellDamage(): number {
    return this.equipment.spellDamage + this.buffStatBonuses.spellDamage
  }

  get arcaneDamage(): number {
    return this.equipment.arcaneDamage + this.buffStatBonuses.arcaneDamage
  }

  get natureDamage(): number {
    return this.equipment.natureDamage + this.buffStatBonuses.natureDamage
  }

  get spellCrit(): number {
    return Math.min(
      constants.spellCritCap,
      this.intellect / 60 + this.equipment.spellCrit + this.buffStatBonuses.spellCrit
    )
  }

  get spellHit(): number {
    return this.equipment.spellHit + this.buffStatBonuses.spellHit
  }

  get saygesDarkFortuneOfDamageBonus(): number {
    return this.hasBuff(Buff.SaygesDarkFortuneOfDamage) ? 1.1 : 1.0
  }

  get thickHideArmorBenefit(): number {
    return this.options.talents.thickHideRank ? this.options.talents.thickHideRank * 0.02 + 1 : 1
  }

  get reflectionBonus(): number {
    switch (this.options.talents.reflectionRank) {
      case 1:
        return 0.05
      case 2:
        return 0.1
      case 3:
        return 0.15
      default:
        return 0
    }
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([key, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          try {
            const val = (this as any)[key]
            jsonObj[key] = val
          } catch (error) {
            console.error(`Error calling getter ${key}`, error)
          }
        }
      })

    return jsonObj
  }
}
