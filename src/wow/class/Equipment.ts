import Item from './Item'
import Character from './Character'
import Simulation from './Simulation'
import Tools from '../module/Tools'
import Locked from '../module/Locked'
import Query from '../module/Query'

import ItemSlot from '../enum/ItemSlot'
import SortOrder from '../enum/SortOrder'

import Options from '../interface/Options'
import LockedItems from '../interface/LockedItems'
import ItemSearch from '../interface/ItemSearch'
import ItemJSON from '../interface/ItemJSON'
import EnchantJSON from '../interface/EnchantJSON'
import ItemSetJSON from '../interface/ItemSetJSON'
import { strict as assert } from 'assert'
import PvPRank from '../enum/PvPRank'
import Weights from '../interface/Weights'

const BASE_PHASE_6_ARMOR_SET = new Map([
  [ItemSlot.Head, 'Guise of the Devourer'],
  [ItemSlot.Neck, `Sadist's Collar`],
  [ItemSlot.Hands, 'Gloves of the Hidden Temple'],
  [ItemSlot.Waist, 'Belt of Never-ending Agony'],
  [ItemSlot.Shoulder, 'Mantle of Wicked Revenge'],
  [ItemSlot.Wrist, 'Qiraji Execution Bracers'],
  [ItemSlot.Legs, 'Leggings of Apocalypse'],
  [ItemSlot.Back, 'Cloak of the Fallen God'],
  [ItemSlot.Feet, 'Boots of the Shadow Flame'],
  [ItemSlot.Chest, `Ghoul Skin Tunic`],
  [ItemSlot.Finger, 'Signet of the Fallen Defender'],
  [ItemSlot.Wrist, ' Qiraji Execution Bracers'],
  [ItemSlot.Finger2, `Master Dragonslayer's Ring`],
  [ItemSlot.Mainhand, `Manual Crowd Pummeler`],
  [ItemSlot.Offhand, 'Tome of Knowledge'],
  [ItemSlot.Trinket, 'Drake Fang Talisman'],
  [ItemSlot.Trinket2, 'Kiss of the Spider'],
  [ItemSlot.Relic, 'Idol of Brutality']
])

const BASE_PHASE_6_ENCHANT_SET = new Map([
  [ItemSlot.Head, 'Arcanum of Rapidity'],
  [ItemSlot.Hands, 'Enchant Gloves - Threat'],
  [ItemSlot.Shoulder, 'Fortitude of the Scourge'],
  [ItemSlot.Wrist, 'Enchant Bracer - Superior Strength'],
  [ItemSlot.Legs, 'Arcanum of Rapidity'],
  [ItemSlot.Back, 'Enchant Cloak - Lesser Agility'],
  [ItemSlot.Feet, 'Enchant Boots - Minor Speed'],
  [ItemSlot.Chest, `Enchant Chest - Greater Stats`],
  [ItemSlot.Mainhand, `Enchant Weapon - Agility`]
])

export default class Equipment {
  options: Options
  itemSearch: ItemSearch
  head: Item
  hands: Item
  neck: Item
  waist: Item
  shoulder: Item
  legs: Item
  back: Item
  feet: Item
  chest: Item
  finger: Item
  wrist: Item
  finger2: Item
  mainhand: Item
  offhand: Item
  trinket: Item
  trinket2: Item
  idol: Item
  artificialStatBonuses: Item
  onUseStatBonuses: Item
  itemSetBonuses: Item

  /* TODO: can I make it so the constructor could take list of item ids or something instead? */
  constructor(options: Options) {
    console.log('Creating equipment')
    this.options = options
    this.artificialStatBonuses = Equipment.createArtificialItem()
    this.onUseStatBonuses = Equipment.createArtificialItem()
    this.itemSearch = Equipment.itemSearchFromOptions(options)

    // If the item/enchant is not locked set it to the default item/enchant
    const _loadDefault = (slot: ItemSlot) => {
      let item = Locked.GetItem(this.itemSearch.lockedItems, slot)
      if (!item) {
        item = Query.Item({
          name: BASE_PHASE_6_ARMOR_SET.get(slot)
        })
      }
      console.assert(item !== undefined, `Unable to load default for slot ${slot}`)
      let enchant = Locked.GetEnchant(this.itemSearch.lockedEnchants, slot)
      if (enchant === undefined && BASE_PHASE_6_ENCHANT_SET.has(slot)) {
        enchant = Query.Enchants({
          name: BASE_PHASE_6_ENCHANT_SET.get(slot),
          slot: slot
        })[0]
      }

      return new Item(slot, item, enchant)
    }

    this.head = _loadDefault(ItemSlot.Head)
    this.hands = _loadDefault(ItemSlot.Hands)
    this.neck = _loadDefault(ItemSlot.Neck)
    this.waist = _loadDefault(ItemSlot.Waist)
    this.shoulder = _loadDefault(ItemSlot.Shoulder)
    this.legs = _loadDefault(ItemSlot.Legs)
    this.back = _loadDefault(ItemSlot.Back)
    this.feet = _loadDefault(ItemSlot.Feet)
    this.chest = _loadDefault(ItemSlot.Chest)
    this.wrist = _loadDefault(ItemSlot.Wrist)
    this.finger = _loadDefault(ItemSlot.Finger)
    this.finger2 = _loadDefault(ItemSlot.Finger2)
    if (this.finger.name === this.finger2.name && this.finger.isUnique) {
      this.finger2 = new Item(ItemSlot.Finger2, Locked.emptyItem(ItemSlot.Finger2), undefined)
    }
    this.mainhand = _loadDefault(ItemSlot.Mainhand)
    this.offhand = _loadDefault(ItemSlot.Offhand)
    if (this.mainhand.isTwoHander) {
      this.offhand = new Item(ItemSlot.Offhand, Locked.emptyItem(ItemSlot.Offhand), undefined)
    }
    this.trinket = _loadDefault(ItemSlot.Trinket)
    this.trinket2 = _loadDefault(ItemSlot.Trinket2)
    if (this.trinket.name === this.trinket2.name && this.trinket.isUnique) {
      this.trinket2 = new Item(ItemSlot.Trinket2, Locked.emptyItem(ItemSlot.Trinket2), undefined)
    }
    this.idol = _loadDefault(ItemSlot.Relic)

    Locked.LockItems(this.itemSearch.lockedItems, this)

    // TODO: After computing item set bonus, set an extra string on items within sets about the bonus
    this.itemSetBonuses = this.computeItemSetBonuses()
  }

  static createArtificialItem(stats: Object = {}): Item {
    return new Item(ItemSlot.None, {
      ...stats,
      slot: ItemSlot.None,
      worldBoss: false,
      raid: false,
      pvpRank: PvPRank.Scout
    })
  }

  static itemSearchFromOptions(options: Options): ItemSearch {
    console.log('itemSearchFromOptions')
    const myOptions: Options = Tools.CloneObject(options)

    return {
      phase: myOptions.phase,
      faction: Character.factionFromRace(myOptions.character.race),
      pvpRank: myOptions.character.pvpRank,
      raids: myOptions.raids,
      worldBosses: myOptions.worldBosses,
      randomEnchants: myOptions.randomEnchants,
      encounterLength: myOptions.encounterLength,
      onUseItems: myOptions.onUseItems,
      targetType: myOptions.target.type,
      lockedItems: myOptions.character.lockedItems,
      lockedEnchants: myOptions.character.lockedEnchants,
      slot: myOptions.itemSearchSlot,
      sortOrder: SortOrder.Descending
    }
  }

  static optimalItemsForSlot(options: Options, itemSearch: ItemSearch, tpsWeights: Weights, ehpWeights: Weights) {
    /* itemSearchSlot is only set when a user clicks a slot to equip an item. If that's not
     * the case then we don't need to do anything */
    const slot = options.itemSearchSlot
    if (slot === ItemSlot.None) {
      return undefined
    }

    /* We need the stat weights MINUS the slot we're getting items for. So make a private
     * copy of options, unequip the slot, and run the equipment optimization function.
     * Our stat weights will be contained in the itemSearch. */
    const tmpItemSearch: ItemSearch = Tools.CloneObject(itemSearch)

    /* and finally retrieve the items for this slot, using the weights
     * we just got. Copy the original version of what we overwrote above
     * and unlock the slot so it doesn't return a user locked item */
    tmpItemSearch.lockedItems = Tools.CloneObject(options.character.lockedItems)
    Locked.UnlockItem(tmpItemSearch.lockedItems, options.itemSearchSlot)
    return Equipment.getWeightedItemsBySlot(slot, tmpItemSearch, tpsWeights, ehpWeights)
  }

  static optimalEnchantsForSlot(options: Options, itemSearch: ItemSearch, tpsWeights: Weights, ehpWeights: Weights) {
    /* Same process as above, but for enchants */
    const slot = options.enchantSearchSlot
    if (slot === ItemSlot.None) {
      return undefined
    }

    const tmpItemSearch: ItemSearch = Tools.CloneObject(itemSearch)

    tmpItemSearch.lockedEnchants = Tools.CloneObject(options.character.lockedEnchants)
    Locked.UnlockEnchant(tmpItemSearch.lockedEnchants, options.enchantSearchSlot)
    return Equipment.getWeightedEnchantsBySlot(slot, tmpItemSearch, tpsWeights, ehpWeights)
  }

  static optimalEquipment(options: Options) {
    const myOptions = Tools.CloneObject(options)

    const ehpStatWeights: Weights | undefined = undefined
    const tpsStatWeights: Weights | undefined = undefined
    console.debug(`EHP Weight ${JSON.stringify(ehpStatWeights)} \n TPS Weights ${JSON.stringify(tpsStatWeights)}`)

    const simulation = new Simulation(myOptions)
    const tempTPSStatWeights = simulation.tpsStatWeights()
    if (simulation.character.meleeHit > simulation.playerMeleeHitCap) {
      tempTPSStatWeights.meleeHit =
        tempTPSStatWeights.meleeHit / ((simulation.character.meleeHit - simulation.playerMeleeHitCap) * 100)
    }

    return simulation.character.equipment
  }

  static printItemNames(equipment: Equipment) {
    console.log(`
      ${equipment.head.name}, ${equipment.hands.name}, ${equipment.neck.name},
      ${equipment.waist.name}, ${equipment.shoulder.name}, ${equipment.legs.name},
      ${equipment.back.name}, ${equipment.feet.name}, ${equipment.chest.name},
      ${equipment.finger.name}, ${equipment.wrist.name}, ${equipment.finger2.name},
      ${equipment.mainhand.name}, ${equipment.trinket.name}, ${equipment.offhand.name},
      ${equipment.trinket2.name}, ${equipment.idol.spellDamage}`)
  }

  static isUniqueEquip(itemJSON: ItemJSON) {
    return itemJSON.unique || (itemJSON.boss && itemJSON.boss.includes('Quest:')) ? true : false
  }

  static isOnUseEquip(itemJSON: ItemJSON | undefined) {
    return itemJSON && itemJSON.onUse ? true : false
  }

  static getWeightedItemsBySlot(
    slot: ItemSlot,
    itemSearch: ItemSearch,
    tpsWeights: Weights,
    ehpWeights: Weights
  ): ItemJSON[] {
    const lockedItem = Locked.GetItem(itemSearch.lockedItems, slot)
    if (lockedItem) {
      const x = []
      lockedItem.score = Item.scoreItem(lockedItem, ehpWeights, tpsWeights)
      lockedItem.threatScore = Item.threatScore(tpsWeights, lockedItem)
      lockedItem.mitScore = Item.mitScore(ehpWeights, lockedItem)
      x.push(lockedItem)
      return x
    }

    const result = Query.Items({
      cloneResults: true,
      slot: slot,
      phase: itemSearch.phase,
      faction: itemSearch.faction,
      pvpRank: itemSearch.pvpRank,
      worldBosses: itemSearch.worldBosses,
      raids: itemSearch.raids,
      randomEnchants: itemSearch.randomEnchants
    })

    /* score items */
    for (let i = 0; i < result.length; i++) {
      const score = Item.scoreItem(result[i], ehpWeights, tpsWeights)
      const threatScore = Item.threatScore(tpsWeights, result[i])
      const mitScore = Item.mitScore(ehpWeights, result[i])
      result[i].score = score
      result[i].mitScore = mitScore
      result[i].threatScore = threatScore
    }

    result.sort(itemSearch.sortOrder === SortOrder.Descending ? Item.sortScoreDes : Item.sortScoreAsc)
    return result
  }

  static getWeightedEnchantsBySlot(slot: ItemSlot, itemSearch: ItemSearch, tpsWeights: Weights, ehpWeights: Weights) {
    const lockedEnchant = Locked.GetEnchant(itemSearch.lockedEnchants, slot)
    /// Invalidate the locked enchant if the locked enchant is not compatible
    //  with weapon to be equipped or from a phase later than the current search
    // TODO: Do I unlock the enchant?
    const invalidateLockedEnchant =
      lockedEnchant !== undefined &&
      lockedEnchant.phase <= itemSearch.phase &&
      (lockedEnchant.applicableWeaponSubclasses === undefined ||
        ('applicableWeaponSubclasses' in lockedEnchant &&
          lockedEnchant.applicableWeaponSubclasses.includes(
            itemSearch.equippedWeaponSubclass ? itemSearch.equippedWeaponSubclass : -100
          )))

    if (lockedEnchant !== undefined && invalidateLockedEnchant) {
      const x = []
      lockedEnchant.score = Item.scoreEnchant(lockedEnchant, ehpWeights, tpsWeights)
      const mitScore = Item.mitScore(ehpWeights, lockedEnchant)
      const threatScore = Item.threatScore(tpsWeights, lockedEnchant)
      lockedEnchant.mitScore = mitScore
      lockedEnchant.threatScore = threatScore
      x.push(lockedEnchant)
      return x
    }

    const result = Query.Enchants({
      cloneResults: true,
      slot: slot,
      phase: itemSearch.phase,
      equippedWeaponSubclass: itemSearch.equippedWeaponSubclass
    })

    for (const i in result) {
      const mitScore = Item.mitScore(ehpWeights, result[i])
      const threatScore = Item.threatScore(tpsWeights, result[i])
      const score = Item.scoreEnchant(result[i], ehpWeights, tpsWeights)
      result[i].score = score
      result[i].mitScore = mitScore
      result[i].threatScore = threatScore
    }
    result.sort(itemSearch.sortOrder === SortOrder.Descending ? Item.sortScoreDes : Item.sortScoreAsc)
    return result
  }

  computeItemSetBonuses(): Item {
    const itemSets: Array<ItemSetJSON> = Query.ItemSets({
      cloneResults: false
    })
    if (!itemSets || !itemSets[0]) {
      return Equipment.createArtificialItem()
    }

    let bonuses = {}
    for (const itemSet of itemSets) {
      let numPieces = 0
      const setPieces: Array<Item> = []
      if (itemSet.itemNames.includes(this.head.name)) {
        numPieces += 1
        setPieces.push(this.head)
      }
      if (itemSet.itemNames.includes(this.hands.name)) {
        numPieces += 1
        setPieces.push(this.hands)
      }
      if (itemSet.itemNames.includes(this.neck.name)) {
        numPieces += 1
        setPieces.push(this.neck)
      }
      if (itemSet.itemNames.includes(this.waist.name)) {
        numPieces += 1
        setPieces.push(this.waist)
      }
      if (itemSet.itemNames.includes(this.shoulder.name)) {
        numPieces += 1
        setPieces.push(this.shoulder)
      }
      if (itemSet.itemNames.includes(this.legs.name)) {
        numPieces += 1
        setPieces.push(this.legs)
      }
      if (itemSet.itemNames.includes(this.back.name)) {
        numPieces += 1
        setPieces.push(this.back)
      }
      if (itemSet.itemNames.includes(this.feet.name)) {
        numPieces += 1
        setPieces.push(this.feet)
      }
      if (itemSet.itemNames.includes(this.chest.name)) {
        numPieces += 1
        setPieces.push(this.chest)
      }
      if (itemSet.itemNames.includes(this.finger.name)) {
        numPieces += 1
        setPieces.push(this.finger)
      }
      if (itemSet.itemNames.includes(this.wrist.name)) {
        numPieces += 1
        setPieces.push(this.wrist)
      }
      if (itemSet.itemNames.includes(this.finger2.name)) {
        numPieces += 1
        setPieces.push(this.finger2)
      }
      if (itemSet.itemNames.includes(this.mainhand.name)) {
        numPieces += 1
        setPieces.push(this.mainhand)
      }
      if (itemSet.itemNames.includes(this.offhand.name)) {
        numPieces += 1
        setPieces.push(this.offhand)
      }
      if (itemSet.itemNames.includes(this.trinket.name)) {
        numPieces += 1
        setPieces.push(this.trinket)
      }
      if (itemSet.itemNames.includes(this.trinket2.name)) {
        numPieces += 1
        setPieces.push(this.trinket2)
      }
      if (itemSet.itemNames.includes(this.idol.name)) {
        numPieces += 1
        setPieces.push(this.idol)
      }

      // Aggregate all active bonus effects into the bonuses object

      // And save off the info about which current set bonuses are active and inactive
      // (and format them nicely in strings) for display on the item tooltip
      // as well as saving of which pieces from the set are currently equipped
      const activeSetBonusValues: Array<string> = []
      const inactiveSetBonusValues: Array<string> = []
      for (const [key, value] of Object.entries(itemSet).sort((a, b) => a[1].pieces - b[1].pieces)) {
        if (value.pieces !== undefined && value.pieces > numPieces) {
          inactiveSetBonusValues.push(`(${value.pieces}) ${Item.formatStatBonus(key, value.bonus, true)}`)
        }
        if (value.pieces !== undefined && value.pieces <= numPieces) {
          bonuses = { [key]: value.bonus, ...bonuses }
          activeSetBonusValues.push(`(${value.pieces}) ${Item.formatStatBonus(key, value.bonus, true)}`)
        }
      }
      if (numPieces) {
        const setItemsEquipped: Set<string> = new Set()
        const allSetItems = new Set(itemSet.itemNames)
        for (const setPiece of setPieces) {
          setItemsEquipped.add(setPiece.name)
        }
        const setItemsUnequipped: Set<string> = Tools.difference(allSetItems, setItemsEquipped)

        for (const setPiece of setPieces) {
          setPiece.setBonusInfo.setName = itemSet.name
          setPiece.setBonusInfo.setItemsEquipped = setItemsEquipped
          setPiece.setBonusInfo.setItemsUnequipped = setItemsUnequipped
          setPiece.setBonusInfo.activeSetBonusValues = activeSetBonusValues
          setPiece.setBonusInfo.inactiveSetBonusValues = inactiveSetBonusValues
        }
      }
    }
    return Equipment.createArtificialItem(bonuses)
  }

  get spellDamage(): number {
    return (
      this.head.spellDamage +
      this.hands.spellDamage +
      this.neck.spellDamage +
      this.waist.spellDamage +
      this.shoulder.spellDamage +
      this.legs.spellDamage +
      this.back.spellDamage +
      this.feet.spellDamage +
      this.chest.spellDamage +
      this.finger.spellDamage +
      this.wrist.spellDamage +
      this.finger2.spellDamage +
      this.mainhand.spellDamage +
      this.offhand.spellDamage +
      this.trinket.spellDamage +
      this.trinket2.spellDamage +
      this.idol.spellDamage +
      this.artificialStatBonuses.spellDamage +
      this.onUseStatBonuses.spellDamage +
      this.itemSetBonuses.spellDamage
    )
  }

  get arcaneDamage(): number {
    return (
      this.head.arcaneDamage +
      this.hands.arcaneDamage +
      this.neck.arcaneDamage +
      this.waist.arcaneDamage +
      this.shoulder.arcaneDamage +
      this.legs.arcaneDamage +
      this.back.arcaneDamage +
      this.feet.arcaneDamage +
      this.chest.arcaneDamage +
      this.finger.arcaneDamage +
      this.wrist.arcaneDamage +
      this.finger2.arcaneDamage +
      this.mainhand.arcaneDamage +
      this.offhand.arcaneDamage +
      this.trinket.arcaneDamage +
      this.trinket2.arcaneDamage +
      this.idol.arcaneDamage +
      this.artificialStatBonuses.arcaneDamage +
      this.onUseStatBonuses.arcaneDamage +
      this.itemSetBonuses.arcaneDamage
    )
  }

  get natureDamage(): number {
    return (
      this.head.natureDamage +
      this.hands.natureDamage +
      this.neck.natureDamage +
      this.waist.natureDamage +
      this.shoulder.natureDamage +
      this.legs.natureDamage +
      this.back.natureDamage +
      this.feet.natureDamage +
      this.chest.natureDamage +
      this.finger.natureDamage +
      this.wrist.natureDamage +
      this.finger2.natureDamage +
      this.mainhand.natureDamage +
      this.offhand.natureDamage +
      this.trinket.natureDamage +
      this.trinket2.natureDamage +
      this.idol.natureDamage +
      this.artificialStatBonuses.natureDamage +
      this.onUseStatBonuses.natureDamage +
      this.itemSetBonuses.natureDamage
    )
  }

  get spellHit(): number {
    return (
      this.head.spellHit +
      this.hands.spellHit +
      this.neck.spellHit +
      this.waist.spellHit +
      this.shoulder.spellHit +
      this.legs.spellHit +
      this.back.spellHit +
      this.feet.spellHit +
      this.chest.spellHit +
      this.finger.spellHit +
      this.wrist.spellHit +
      this.finger2.spellHit +
      this.mainhand.spellHit +
      this.offhand.spellHit +
      this.trinket.spellHit +
      this.trinket2.spellHit +
      this.idol.spellHit +
      this.artificialStatBonuses.spellHit +
      this.onUseStatBonuses.spellHit +
      this.itemSetBonuses.spellHit
    )
  }

  get spellCrit(): number {
    return (
      this.head.spellCrit +
      this.hands.spellCrit +
      this.neck.spellCrit +
      this.waist.spellCrit +
      this.shoulder.spellCrit +
      this.legs.spellCrit +
      this.back.spellCrit +
      this.feet.spellCrit +
      this.chest.spellCrit +
      this.finger.spellCrit +
      this.wrist.spellCrit +
      this.finger2.spellCrit +
      this.mainhand.spellCrit +
      this.offhand.spellCrit +
      this.trinket.spellCrit +
      this.trinket2.spellCrit +
      this.idol.spellCrit +
      this.artificialStatBonuses.spellCrit +
      this.onUseStatBonuses.spellCrit +
      this.itemSetBonuses.spellCrit
    )
  }

  get intellect(): number {
    return (
      this.head.intellect +
      this.hands.intellect +
      this.neck.intellect +
      this.waist.intellect +
      this.shoulder.intellect +
      this.legs.intellect +
      this.back.intellect +
      this.feet.intellect +
      this.chest.intellect +
      this.finger.intellect +
      this.wrist.intellect +
      this.finger2.intellect +
      this.mainhand.intellect +
      this.offhand.intellect +
      this.trinket.intellect +
      this.trinket2.intellect +
      this.idol.intellect +
      this.artificialStatBonuses.intellect +
      this.onUseStatBonuses.intellect +
      this.itemSetBonuses.intellect
    )
  }

  get stamina(): number {
    return (
      this.head.stamina +
      this.hands.stamina +
      this.neck.stamina +
      this.waist.stamina +
      this.shoulder.stamina +
      this.legs.stamina +
      this.back.stamina +
      this.feet.stamina +
      this.chest.stamina +
      this.finger.stamina +
      this.wrist.stamina +
      this.finger2.stamina +
      this.mainhand.stamina +
      this.offhand.stamina +
      this.trinket.stamina +
      this.trinket2.stamina +
      this.idol.stamina +
      this.artificialStatBonuses.stamina +
      this.onUseStatBonuses.stamina +
      this.itemSetBonuses.stamina
    )
  }

  get spirit(): number {
    return (
      this.head.spirit +
      this.hands.spirit +
      this.neck.spirit +
      this.waist.spirit +
      this.shoulder.spirit +
      this.legs.spirit +
      this.back.spirit +
      this.feet.spirit +
      this.chest.spirit +
      this.finger.spirit +
      this.wrist.spirit +
      this.finger2.spirit +
      this.mainhand.spirit +
      this.offhand.spirit +
      this.trinket.spirit +
      this.trinket2.spirit +
      this.idol.spirit +
      this.artificialStatBonuses.spirit +
      this.onUseStatBonuses.spirit +
      this.itemSetBonuses.spirit
    )
  }

  get mp5(): number {
    return (
      this.head.mp5 +
      this.hands.mp5 +
      this.neck.mp5 +
      this.waist.mp5 +
      this.shoulder.mp5 +
      this.legs.mp5 +
      this.back.mp5 +
      this.feet.mp5 +
      this.chest.mp5 +
      this.finger.mp5 +
      this.wrist.mp5 +
      this.finger2.mp5 +
      this.mainhand.mp5 +
      this.offhand.mp5 +
      this.trinket.mp5 +
      this.trinket2.mp5 +
      this.idol.mp5 +
      this.artificialStatBonuses.mp5 +
      this.onUseStatBonuses.mp5 +
      this.itemSetBonuses.mp5
    )
  }

  get strength(): number {
    return (
      this.head.strength +
      this.hands.strength +
      this.neck.strength +
      this.waist.strength +
      this.shoulder.strength +
      this.legs.strength +
      this.back.strength +
      this.feet.strength +
      this.chest.strength +
      this.finger.strength +
      this.wrist.strength +
      this.finger2.strength +
      this.mainhand.strength +
      this.offhand.strength +
      this.trinket.strength +
      this.trinket2.strength +
      this.idol.strength +
      this.artificialStatBonuses.strength +
      this.onUseStatBonuses.strength +
      this.itemSetBonuses.strength
    )
  }

  get agility(): number {
    return (
      this.head.agility +
      this.hands.agility +
      this.neck.agility +
      this.waist.agility +
      this.shoulder.agility +
      this.legs.agility +
      this.back.agility +
      this.feet.agility +
      this.chest.agility +
      this.finger.agility +
      this.wrist.agility +
      this.finger2.agility +
      this.mainhand.agility +
      this.offhand.agility +
      this.trinket.agility +
      this.trinket2.agility +
      this.idol.agility +
      this.artificialStatBonuses.agility +
      this.onUseStatBonuses.agility +
      this.itemSetBonuses.agility
    )
  }

  // Between 0 and 1
  get meleeHit(): number {
    return (
      (this.head.meleeHit +
        this.hands.meleeHit +
        this.neck.meleeHit +
        this.waist.meleeHit +
        this.shoulder.meleeHit +
        this.legs.meleeHit +
        this.back.meleeHit +
        this.feet.meleeHit +
        this.chest.meleeHit +
        this.finger.meleeHit +
        this.wrist.meleeHit +
        this.finger2.meleeHit +
        this.mainhand.meleeHit +
        this.offhand.meleeHit +
        this.trinket.meleeHit +
        this.trinket2.meleeHit +
        this.idol.meleeHit +
        this.artificialStatBonuses.meleeHit +
        this.onUseStatBonuses.meleeHit +
        this.itemSetBonuses.meleeHit) /
      100
    )
  }

  get meleeCrit(): number {
    return (
      (this.head.meleeCrit +
        this.hands.meleeCrit +
        this.neck.meleeCrit +
        this.waist.meleeCrit +
        this.shoulder.meleeCrit +
        this.legs.meleeCrit +
        this.back.meleeCrit +
        this.feet.meleeCrit +
        this.chest.meleeCrit +
        this.finger.meleeCrit +
        this.wrist.meleeCrit +
        this.finger2.meleeCrit +
        this.mainhand.meleeCrit +
        this.offhand.meleeCrit +
        this.trinket.meleeCrit +
        this.trinket2.meleeCrit +
        this.idol.meleeCrit +
        this.artificialStatBonuses.meleeCrit +
        this.onUseStatBonuses.meleeCrit +
        this.itemSetBonuses.meleeCrit) /
      100
    )
  }

  get haste(): number {
    const haste =
      this.head.haste *
      this.hands.haste *
      this.neck.haste *
      this.waist.haste *
      this.shoulder.haste *
      this.legs.haste *
      this.back.haste *
      this.feet.haste *
      this.chest.haste *
      this.finger.haste *
      this.wrist.haste *
      this.finger2.haste *
      this.mainhand.haste *
      this.offhand.haste *
      this.trinket.haste *
      this.trinket2.haste *
      this.idol.haste *
      this.artificialStatBonuses.haste *
      this.onUseStatBonuses.haste *
      this.itemSetBonuses.haste
    return haste
  }

  get attackPower(): number {
    return (
      this.head.attackPower +
      this.hands.attackPower +
      this.neck.attackPower +
      this.waist.attackPower +
      this.shoulder.attackPower +
      this.legs.attackPower +
      this.back.attackPower +
      this.feet.attackPower +
      this.chest.attackPower +
      this.finger.attackPower +
      this.wrist.attackPower +
      this.finger2.attackPower +
      this.mainhand.attackPower +
      this.offhand.attackPower +
      this.trinket.attackPower +
      this.trinket2.attackPower +
      this.idol.attackPower +
      this.artificialStatBonuses.attackPower +
      this.onUseStatBonuses.attackPower +
      this.itemSetBonuses.attackPower
    )
  }

  get feralAttackPower(): number {
    return (
      this.head.feralAttackPower +
      this.hands.feralAttackPower +
      this.neck.feralAttackPower +
      this.waist.feralAttackPower +
      this.shoulder.feralAttackPower +
      this.legs.feralAttackPower +
      this.back.feralAttackPower +
      this.feet.feralAttackPower +
      this.chest.feralAttackPower +
      this.finger.feralAttackPower +
      this.wrist.feralAttackPower +
      this.finger2.feralAttackPower +
      this.mainhand.feralAttackPower +
      this.offhand.feralAttackPower +
      this.trinket.feralAttackPower +
      this.trinket2.feralAttackPower +
      this.idol.feralAttackPower +
      this.artificialStatBonuses.feralAttackPower +
      this.onUseStatBonuses.feralAttackPower +
      this.itemSetBonuses.feralAttackPower
    )
  }

  // Armor for equipment not including bonus armor from set bonuses or enchants
  get armor(): number {
    return (
      this.head.armor +
      this.hands.armor +
      this.neck.armor +
      this.waist.armor +
      this.shoulder.armor +
      this.legs.armor +
      this.back.armor +
      this.feet.armor +
      this.chest.armor +
      this.finger.armor +
      this.wrist.armor +
      this.finger2.armor +
      this.mainhand.armor +
      this.offhand.armor +
      this.trinket.armor +
      this.trinket2.armor +
      this.idol.armor +
      this.artificialStatBonuses.armor +
      this.onUseStatBonuses.armor +
      this.itemSetBonuses.armor
    )
  }

  // Armor gained from enchants and set bonuses (e.g. armor from sources that have green text in game)
  get bonusArmor(): number {
    return (
      this.head.bonusArmor +
      this.hands.bonusArmor +
      this.neck.bonusArmor +
      this.waist.bonusArmor +
      this.shoulder.bonusArmor +
      this.legs.bonusArmor +
      this.back.bonusArmor +
      this.feet.bonusArmor +
      this.chest.bonusArmor +
      this.finger.bonusArmor +
      this.wrist.bonusArmor +
      this.finger2.bonusArmor +
      this.mainhand.bonusArmor +
      this.offhand.bonusArmor +
      this.trinket.bonusArmor +
      this.trinket2.bonusArmor +
      this.idol.bonusArmor +
      this.artificialStatBonuses.bonusArmor +
      this.onUseStatBonuses.bonusArmor +
      this.itemSetBonuses.bonusArmor
    )
  }

  get defense(): number {
    return (
      this.head.defense +
      this.hands.defense +
      this.neck.defense +
      this.waist.defense +
      this.shoulder.defense +
      this.legs.defense +
      this.back.defense +
      this.feet.defense +
      this.chest.defense +
      this.finger.defense +
      this.wrist.defense +
      this.finger2.defense +
      this.mainhand.defense +
      this.offhand.defense +
      this.trinket.defense +
      this.trinket2.defense +
      this.idol.defense +
      this.artificialStatBonuses.defense +
      this.onUseStatBonuses.defense +
      this.itemSetBonuses.defense
    )
  }

  get bonusHealth(): number {
    return (
      this.head.bonusHealth +
      this.hands.bonusHealth +
      this.neck.bonusHealth +
      this.waist.bonusHealth +
      this.shoulder.bonusHealth +
      this.legs.bonusHealth +
      this.back.bonusHealth +
      this.feet.bonusHealth +
      this.chest.bonusHealth +
      this.finger.bonusHealth +
      this.wrist.bonusHealth +
      this.finger2.bonusHealth +
      this.mainhand.bonusHealth +
      this.offhand.bonusHealth +
      this.trinket.bonusHealth +
      this.trinket2.bonusHealth +
      this.idol.bonusHealth +
      this.artificialStatBonuses.bonusHealth +
      this.onUseStatBonuses.bonusHealth +
      this.itemSetBonuses.bonusHealth
    )
  }
  // Between 0 and 1
  get dodge(): number {
    return (
      (this.head.dodge +
        this.hands.dodge +
        this.neck.dodge +
        this.waist.dodge +
        this.shoulder.dodge +
        this.legs.dodge +
        this.back.dodge +
        this.feet.dodge +
        this.chest.dodge +
        this.finger.dodge +
        this.wrist.dodge +
        this.finger2.dodge +
        this.mainhand.dodge +
        this.offhand.dodge +
        this.trinket.dodge +
        this.trinket2.dodge +
        this.idol.dodge +
        this.artificialStatBonuses.dodge +
        this.onUseStatBonuses.dodge +
        this.itemSetBonuses.dodge) /
      100
    )
  }

  // TODO: Move these onto character and model stat caps
  mitScore(ehpWeights: Weights): number {
    return (
      Item.mitScore(ehpWeights, this.head.itemJSON) +
      Item.mitScore(ehpWeights, this.hands.itemJSON) +
      Item.mitScore(ehpWeights, this.neck.itemJSON) +
      Item.mitScore(ehpWeights, this.waist.itemJSON) +
      Item.mitScore(ehpWeights, this.shoulder.itemJSON) +
      Item.mitScore(ehpWeights, this.legs.itemJSON) +
      Item.mitScore(ehpWeights, this.back.itemJSON) +
      Item.mitScore(ehpWeights, this.feet.itemJSON) +
      Item.mitScore(ehpWeights, this.chest.itemJSON) +
      Item.mitScore(ehpWeights, this.finger.itemJSON) +
      Item.mitScore(ehpWeights, this.wrist.itemJSON) +
      Item.mitScore(ehpWeights, this.finger2.itemJSON) +
      Item.mitScore(ehpWeights, this.mainhand.itemJSON) +
      Item.mitScore(ehpWeights, this.offhand.itemJSON) +
      Item.mitScore(ehpWeights, this.trinket.itemJSON) +
      Item.mitScore(ehpWeights, this.trinket2.itemJSON) +
      Item.mitScore(ehpWeights, this.idol.itemJSON) +
      Item.mitScore(ehpWeights, this.itemSetBonuses.itemJSON) +
      (Item.mitScore(ehpWeights, this.head.enchantJSON) +
        Item.mitScore(ehpWeights, this.hands.enchantJSON) +
        Item.mitScore(ehpWeights, this.neck.enchantJSON) +
        Item.mitScore(ehpWeights, this.waist.enchantJSON) +
        Item.mitScore(ehpWeights, this.shoulder.enchantJSON) +
        Item.mitScore(ehpWeights, this.legs.enchantJSON) +
        Item.mitScore(ehpWeights, this.back.enchantJSON) +
        Item.mitScore(ehpWeights, this.feet.enchantJSON) +
        Item.mitScore(ehpWeights, this.chest.enchantJSON) +
        Item.mitScore(ehpWeights, this.finger.enchantJSON) +
        Item.mitScore(ehpWeights, this.wrist.enchantJSON) +
        Item.mitScore(ehpWeights, this.finger2.enchantJSON) +
        Item.mitScore(ehpWeights, this.mainhand.enchantJSON) +
        Item.mitScore(ehpWeights, this.offhand.enchantJSON) +
        Item.mitScore(ehpWeights, this.trinket.enchantJSON) +
        Item.mitScore(ehpWeights, this.trinket2.enchantJSON) +
        Item.mitScore(ehpWeights, this.idol.enchantJSON) +
        Item.mitScore(ehpWeights, this.itemSetBonuses.enchantJSON))
    )
  }

  threatScore(threatWeights: Weights): number {
    return (
      Item.threatScore(threatWeights, this.head.itemJSON) +
      Item.threatScore(threatWeights, this.hands.itemJSON) +
      Item.threatScore(threatWeights, this.neck.itemJSON) +
      Item.threatScore(threatWeights, this.waist.itemJSON) +
      Item.threatScore(threatWeights, this.shoulder.itemJSON) +
      Item.threatScore(threatWeights, this.legs.itemJSON) +
      Item.threatScore(threatWeights, this.back.itemJSON) +
      Item.threatScore(threatWeights, this.feet.itemJSON) +
      Item.threatScore(threatWeights, this.chest.itemJSON) +
      Item.threatScore(threatWeights, this.finger.itemJSON) +
      Item.threatScore(threatWeights, this.wrist.itemJSON) +
      Item.threatScore(threatWeights, this.finger2.itemJSON) +
      Item.threatScore(threatWeights, this.mainhand.itemJSON) +
      Item.threatScore(threatWeights, this.offhand.itemJSON) +
      Item.threatScore(threatWeights, this.trinket.itemJSON) +
      Item.threatScore(threatWeights, this.trinket2.itemJSON) +
      Item.threatScore(threatWeights, this.idol.itemJSON) +
      Item.threatScore(threatWeights, this.itemSetBonuses.itemJSON) +
      (Item.threatScore(threatWeights, this.head.enchantJSON) +
        Item.threatScore(threatWeights, this.hands.enchantJSON) +
        Item.threatScore(threatWeights, this.neck.enchantJSON) +
        Item.threatScore(threatWeights, this.waist.enchantJSON) +
        Item.threatScore(threatWeights, this.shoulder.enchantJSON) +
        Item.threatScore(threatWeights, this.legs.enchantJSON) +
        Item.threatScore(threatWeights, this.back.enchantJSON) +
        Item.threatScore(threatWeights, this.feet.enchantJSON) +
        Item.threatScore(threatWeights, this.chest.enchantJSON) +
        Item.threatScore(threatWeights, this.finger.enchantJSON) +
        Item.threatScore(threatWeights, this.wrist.enchantJSON) +
        Item.threatScore(threatWeights, this.finger2.enchantJSON) +
        Item.threatScore(threatWeights, this.mainhand.enchantJSON) +
        Item.threatScore(threatWeights, this.offhand.enchantJSON) +
        Item.threatScore(threatWeights, this.trinket.enchantJSON) +
        Item.threatScore(threatWeights, this.trinket2.enchantJSON) +
        Item.threatScore(threatWeights, this.idol.enchantJSON) +
        Item.threatScore(threatWeights, this.itemSetBonuses.enchantJSON))
    )
  }
}
