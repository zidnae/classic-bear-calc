### [Classic Bear Calculator](https://zidnae.gitlab.io/classic-bear-calc/)

### Current Limitations

- Jom Gabbar, Zandalarian Hero Medallion are not modeled
- Item database has most relevent items but is not exhaustive

### TODO

- Replace item database with https://github.com/ultrabis/db
- Display item set bonuses on gear
- Have Overall score account for stat caps
- Do not hardcode in cosumes and buffs and rather define within list
- Re-run db script for rare pvp set

### Changelog

##### Version 0.1.23
###### Added
- Added Dark Desire and Consecrated Weapon Stone Buffs
- Added Arcanum of Protection enchant


##### Version 0.1.22
###### Fixes
- Fixed bug with Atiesh causing DPS calculations to error
- Atiesh tooltip fixed to now properly class restrict to Druid

##### Version 0.1.21

###### Fixes
- Fixed selection of voracity enchants. Previously if you selected the agility enchant it would still choose the strength enchant due to all the enchants sharing the same ID under the hood

##### Version 0.1.20

###### Improvements
- Got rid of the lock symbols from selected gear (carryover from the moonkin calc where gear gets automatically swaped unless locked)
- Renamed the gear "Unlock" button to "Reset" to better reflect its function reseting the gear back to the default

##### Version 0.1.19

###### Fixes
- Fixed tooltip for Moldar's Moxie
- Added Gensis Set and some fire resist pieces to the item database
- Fixed tooltips for set bonuses. In some cases they were showing "Set: Equp: Some Bonus" instead of "Set: Some Bonus"

##### Version 0.1.18

###### Changes

- Default phase has been bumped to 6

###### Improvements

- Item sets and their bonuses are now displayed within the item tooltips
- Tooltips have been added to various elements, most notably buffs
- Mutually exclusive buffs can no longer be selected at the same time (e.g. you can only have one "Well Fed" buff selected at at time)
- Item sets and their bonuses have been added to the gear item tooltips
- Enemy armor is now modeled. Checkout the _Target_ tab to set the armor debuffs and the _Enemy Stats_ tab to see final enemy armor and damage reduction

###### Fixes

- Armor weights no longer become zero when over armor cap and instead are calculated based on the differenece between armor cap and 1 armor below armor cap
- Blue PVP set bonuses now get correctly applied
- Fixed tooltips for Seal of the Dawn, Mark of the Champion, Hand of Justice, and Atiesh
- Mark of the Champion AP bonus now correctly gets applied to demon type enemies in addition to undead

##### Version 0.1.17

###### Fixes

- The on use haste buffs from Manual Crowd Pummeler and Kiss of the Pummeler now properly stack multiplicatively instead of additively

##### Version 0.1.16

###### Improvements

- Blood Pact Buff added
- Added button to header to save current scores and diff them to updated scores when gear is changed
- Removed Threat and EHP scores. They didn't provide much value as EHP and TPS are the real numbers that ought to be examined and optimized. Overall score is still present as it is a useful way to combine EHP and TPS into a single metric

###### Fixes

- Various items were erroneously flagged as Alliance only are now flagged as both faction

##### Version 0.1.15

###### Fixes

- Rage Model tab now displays values computed using weighted a averaged of the rage model calculations with each combination of on use items active/inactive

##### Version 0.1.14

###### Fixes

- Prayer of Fortitude buff now assumes Priest casting has `Improved Power Word: Fortitude` talent

##### Version 0.1.13

###### Fixes

- Miss and Dodge are now no longer reversed within the Auto Attack, Maul, and Swipe Tabs

###### Improvements

- Tender Wolf Steak and Elixir of Giants buffs added

##### Version 0.1.12

###### Improvements

- Add _Swipe Uptime_ to rage model tab

##### Version 0.1.11

###### Improvements

- Add Mongoose Elixir buff

##### Version 0.1.10

###### Improvements

- Added more items to database

##### Version 0.1.9

###### Fixes

- Fix a bug for on use trinkets when more than one was equipped

##### Version 0.1.8

###### Improvements

- Item auto-optimization has been removed
- Item scores are now broken down by mitigation score and threat score
- The mitigation, threat, and overall scores displayed in the header are now calculated via the summing the respective scores of each individual item

##### Version 0.1.7

###### Fixes

- EHP Stat Weights calculations re-done
  - Under the hood EHP increases for health,armor,avoidance, and crit reduction are done under the hood then EHP weights for primary stats are derived from these values
  - Stat weights now align with NerdEggHead's SixtyUpgrades stat weights
- Inspiration and Improved Lay on Hands calculations have been updated to reflect finds from https://discordapp.com/channels/253205420225724416/253206574787461120/77073269632532481

##### Version 0.1.6

###### Fixes

- Armor on weapons now displays correctly
- EHP calculations now use enemy miss chance in addition to player dodge chance
- +Defense now correctly increases the enemies chance to miss

##### Version 0.1.5

###### Improvements

- Added lock all button
- Added Strength of Earth Totem buff

##### Version 0.1.4

###### Fixes

- Added melee crit stat to Field Marshal gear
- Fix for damage reduction from glancing blows when calculating auto attack attack modifier

##### Version 0.1.3

###### Fixes

- Bonus health and attack power from Dire Bear Form are now correctly being applied
- Base 0.9% crit chance is now correctly being modeled
- There is a flat -20 AP modifier applied to every player's total AP which is now being modeled
- Thick Hide bonus modifier now correctly no longer applies to armor gained from agility
- Damage mitigation from armor now uses Taladril's formula from https://classicwow.live/guides/46/basic-stats-sheet
- Boss attack modifier now correctly accounts for crushing blows

##### Version 0.1.2

###### Improvements

- Performance improvements by reducing the number of calls made to the JsonItemDB to compute item set bonuses

##### Version 0.1.1

###### Fixes

- Player of Fortitude button now appropriately adds stamina bonus
- Knight-Captain's Dragonhide Leggings armor value fixed
- Haste now correctly stacks multiplicatively instead of additively

##### Version 0.1.0

- Initial Release

### Project setup

```
yarn install
```

#### Compiles and hot-reloads for development

```
yarn serve
```

#### Compiles and minifies for production

```
yarn build
```

#### Lints and fixes files

```
yarn lint
```

#### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
